#include <iostream>
#include <map>
#include <thread>
#include <set>
#include <mutex>
#include <chrono>
#include <queue>
#include <vector>
#include <algorithm>
#include <fstream>
#include <time.h>
#include <concurrent_unordered_map.h>
#include <deque>

extern "C" {
#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"
}

using namespace std;
using namespace chrono;
#include <WS2tcpip.h>
#pragma comment(lib, "Ws2_32.lib")
#pragma comment(lib, "lua53.lib")

#include <sqlext.h>
#include "protocol.h"

#define _CRT_SECURE_NO_WARNINGS

#define MAX_BUFFER        1024
constexpr auto VIEW_RANGE = 5;

enum EVENT_TYPE { EV_RECV, EV_SEND, EV_MOVE, EV_PLAYER_MOVE_NOTIFY, EV_PLAYER_MOVE, EV_MOVE_TARGET, EV_ATTACK, EV_DAMAGE, EV_NPC_ATTACK, EV_HEAL, EV_ALIVE, EV_CHAT };
enum MONSTER_TYPE { LEVEL1 = 1, LEVEL2 = 3, LEVEL3 = 5, LEVEL4 = 7, NEUTRAL = 0 };

struct OVER_EX {
	WSAOVERLAPPED over;
	WSABUF	wsabuf[1];
	char	net_buf[MAX_BUFFER];	
	int		event_type;
};

struct SOCKETINFO
{
	OVER_EX	recv_over;
	SOCKET	socket;
	int		id;
	int		target;
	int		type;

	bool is_connected;
	bool is_active;
	bool is_move;
	bool is_attack;
	bool is_damage;
	short x, y;
	short c_hp;
	short m_hp;
	short damage;
	short level;
	int c_exp;
	int m_exp;

	set <int> near_id;
	mutex near_lock;
	lua_State *L;
	mutex vm_lock;
};

struct EVENT {
	int obj_id;
	high_resolution_clock::time_point wakeup_time;
	int event_type;
	int target_obj;

	constexpr bool operator < (const EVENT& left) const
	{
		return wakeup_time > left.wakeup_time;
	}
};

struct CHAT {
	int id;
	int month;
	int day;
	int hour;
	int min;
	int sec;
	char chat[50];
};

deque<CHAT> g_dchat;
bool g_bchat = false;

int is_connected[100] = { 0, };

char g_map[WORLD_WIDTH][WORLD_HEIGHT];
ifstream in{ "map.txt" };

priority_queue <EVENT> timer_queue;
mutex  timer_lock;

Concurrency::concurrent_unordered_map <int, SOCKETINFO *> clients;
HANDLE	g_iocp;

///////////////////////////////////
int new_user_id = 0;
//////////////////////////////////

SQLRETURN retcode;
SQLINTEGER nID, nX, nY, nLEVEL, nHP, nEXP;
SQLLEN cbID = 0, cbX = 0, cbY = 0, cbLEVEL, cbEXP, cbHP;
constexpr auto LEN = 10;

ofstream out("채팅 내역.txt", ios::app);

void HandleDiagnosticRecord(SQLHANDLE hHandle, SQLSMALLINT hType, RETCODE RetCode)
{
	SQLSMALLINT iRec = 0;
	SQLINTEGER  iError;
	WCHAR       wszMessage[1000];
	WCHAR       wszState[SQL_SQLSTATE_SIZE + 1];

	if (RetCode == SQL_INVALID_HANDLE) {
		fwprintf(stderr, L"Invalid handle!\n");
		return;
	}

	while (SQLGetDiagRec(hType, hHandle, ++iRec, wszState, &iError, wszMessage,
		(SQLSMALLINT)(sizeof(wszMessage) / sizeof(WCHAR)), (SQLSMALLINT *)NULL) == SQL_SUCCESS) {
		// Hide data truncated.. 
		if (wcsncmp(wszState, L"01004", 5)) {
			fwprintf(stderr, L"[%5.5s] %s (%d)\n", wszState, wszMessage, iError);
		}
	}
	//while (true);
}

void show_error() {
	printf("error\n");
}

void InitPos_DB(int user_id)
{
	setlocale(LC_ALL, "korean");
	SQLHENV henv;
	SQLHDBC hdbc;
	SQLHSTMT hstmt = 0;

	// Allocate environment handle  
	retcode = SQLAllocHandle(SQL_HANDLE_ENV, SQL_NULL_HANDLE, &henv);

	// Set the ODBC version environment attribute  
	if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {
		retcode = SQLSetEnvAttr(henv, SQL_ATTR_ODBC_VERSION, (SQLPOINTER*)SQL_OV_ODBC3, 0);

		// Allocate connection handle  
		if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {
			retcode = SQLAllocHandle(SQL_HANDLE_DBC, henv, &hdbc);

			// Set login timeout to 5 seconds  
			if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {
				SQLSetConnectAttr(hdbc, SQL_LOGIN_TIMEOUT, (SQLPOINTER)5, 0);

				// Connect to data source  
				retcode = SQLConnect(hdbc, (SQLWCHAR*)L"2015182040_CJR_PROJECT", SQL_NTS, (SQLWCHAR*)NULL, SQL_NTS, NULL, SQL_NTS);

				// Allocate statement handle  
				if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {
					retcode = SQLAllocHandle(SQL_HANDLE_STMT, hdbc, &hstmt);		// 명령어 받을 핸들 할당. hstmt.

					SQLWCHAR query[1024];
					//wsprintf(query, L"UPDATE player_table SET c_px = %d, c_py = %d WHERE c_key = %d", x, y, keyid);
					wsprintf(query, L"EXEC get_inform %d", user_id);
					retcode = SQLExecDirect(hstmt, (SQLWCHAR *)query, SQL_NTS);
					if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {

						retcode = SQLBindCol(hstmt, 1, SQL_C_LONG, &nX, LEN, &cbX);	// SQL_C_LONG == SQL_C_INTEGER
						retcode = SQLBindCol(hstmt, 2, SQL_C_LONG, &nY, LEN, &cbY);
						retcode = SQLBindCol(hstmt, 3, SQL_C_LONG, &nLEVEL, LEN, &cbLEVEL);
						retcode = SQLBindCol(hstmt, 4, SQL_C_LONG, &nHP, LEN, &cbHP);
						retcode = SQLBindCol(hstmt, 5, SQL_C_LONG, &nEXP, LEN, &cbEXP);

						// Fetch and print each row of data. On an error, display a message and exit.  
						for (int i = 0; ; i++) {
							retcode = SQLFetch(hstmt);	// SQLFetch 를 통해 데이터를 꺼낸다.
							// SUCCESS이거나 ERROR가 출력. 둘 다 아니면 끝난거다. 빠져 나가자.
							if (retcode == SQL_ERROR)
								show_error();
							if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO);
								//wprintf(L"%d %d %d %d %d %d\n", user_id, nX, nY, nLEVEL, nHP, nEXP);
							else
								break;
						}
					}
					else {
						HandleDiagnosticRecord(hstmt, SQL_HANDLE_STMT, retcode);
					}

					// Process data  
					if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {
						SQLCancel(hstmt);
						SQLFreeHandle(SQL_HANDLE_STMT, hstmt);
					}

					SQLDisconnect(hdbc);
				}

				SQLFreeHandle(SQL_HANDLE_DBC, hdbc);
			}
		}
		SQLFreeHandle(SQL_HANDLE_ENV, henv);
	}
}

void SavePos_DB(int x, int y, int level, int hp, int exp, int id)
{
	setlocale(LC_ALL, "korean");
	SQLHENV henv;
	SQLHDBC hdbc;
	SQLHSTMT hstmt = 0;

	// Allocate environment handle  
	retcode = SQLAllocHandle(SQL_HANDLE_ENV, SQL_NULL_HANDLE, &henv);

	// Set the ODBC version environment attribute  
	if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {
		retcode = SQLSetEnvAttr(henv, SQL_ATTR_ODBC_VERSION, (SQLPOINTER*)SQL_OV_ODBC3, 0);

		// Allocate connection handle  
		if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {
			retcode = SQLAllocHandle(SQL_HANDLE_DBC, henv, &hdbc);

			// Set login timeout to 5 seconds  
			if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {
				SQLSetConnectAttr(hdbc, SQL_LOGIN_TIMEOUT, (SQLPOINTER)5, 0);

				// Connect to data source  
				retcode = SQLConnect(hdbc, (SQLWCHAR*)L"2015182040_CJR_PROJECT", SQL_NTS, (SQLWCHAR*)NULL, SQL_NTS, NULL, SQL_NTS);

				// Allocate statement handle  
				if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {
					retcode = SQLAllocHandle(SQL_HANDLE_STMT, hdbc, &hstmt);		// 명령어 받을 핸들 할당. hstmt.

					SQLWCHAR query[1024];
					wsprintf(query, L"UPDATE User_Table SET c_x = %d, c_y = %d, c_level = %d, c_hp = %d, c_exp = %d WHERE c_id = %d", x, y, level, hp, exp, id);
					retcode = SQLExecDirect(hstmt, (SQLWCHAR *)query, SQL_NTS);
					if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {
						printf("DB 저장 성공, ID : %d\n", id);
					}
					else {
						HandleDiagnosticRecord(hstmt, SQL_HANDLE_STMT, retcode);
					}

					// Process data  
					if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {
						SQLCancel(hstmt);
						SQLFreeHandle(SQL_HANDLE_STMT, hstmt);
					}

					SQLDisconnect(hdbc);
				}

				SQLFreeHandle(SQL_HANDLE_DBC, hdbc);
			}
		}
		SQLFreeHandle(SQL_HANDLE_ENV, henv);
	}
}

void Check_ID_DB()
{
	setlocale(LC_ALL, "korean");
	SQLHENV henv;
	SQLHDBC hdbc;
	SQLHSTMT hstmt = 0;

	// Allocate environment handle  
	retcode = SQLAllocHandle(SQL_HANDLE_ENV, SQL_NULL_HANDLE, &henv);

	// Set the ODBC version environment attribute  
	if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {
		retcode = SQLSetEnvAttr(henv, SQL_ATTR_ODBC_VERSION, (SQLPOINTER*)SQL_OV_ODBC3, 0);

		// Allocate connection handle  
		if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {
			retcode = SQLAllocHandle(SQL_HANDLE_DBC, henv, &hdbc);

			// Set login timeout to 5 seconds  
			if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {
				SQLSetConnectAttr(hdbc, SQL_LOGIN_TIMEOUT, (SQLPOINTER)5, 0);

				// Connect to data source  
				retcode = SQLConnect(hdbc, (SQLWCHAR*)L"2015182040_CJR_PROJECT", SQL_NTS, (SQLWCHAR*)NULL, SQL_NTS, NULL, SQL_NTS);

				// Allocate statement handle  
				if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {
					retcode = SQLAllocHandle(SQL_HANDLE_STMT, hdbc, &hstmt);		// 명령어 받을 핸들 할당. hstmt.

					retcode = SQLExecDirect(hstmt, (SQLWCHAR *)L"SELECT c_id FROM User_Table", SQL_NTS);
					if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {
						// SQLBindCol를 이용해서 변수에 저장해라.
						retcode = SQLBindCol(hstmt, 1, SQL_C_LONG, &nID, 10, &cbID);
						// Fetch and print each row of data. On an error, display a message and exit.  
						for (int i = 0; ; i++) {
							retcode = SQLFetch(hstmt);	// SQLFetch 를 통해 데이터를 꺼낸다.
							// SUCCESS이거나 ERROR가 출력. 둘 다 아니면 끝난거다. 빠져 나가자.
							if (retcode == SQL_ERROR)
								show_error();
							if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {
								wprintf(L"%d: %d\n", i + 1, nID);
								is_connected[i] = nID;
								break;
							}
							else
								break;
						}
					}
					else {
						HandleDiagnosticRecord(hstmt, SQL_HANDLE_STMT, retcode);
					}

					// Process data  
					if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {
						SQLCancel(hstmt);
						SQLFreeHandle(SQL_HANDLE_STMT, hstmt);
					}

					SQLDisconnect(hdbc);
				}

				SQLFreeHandle(SQL_HANDLE_DBC, hdbc);
			}
		}
		SQLFreeHandle(SQL_HANDLE_ENV, henv);
	}
}

void Make_ID_DB(int id, int x, int y)
{
	setlocale(LC_ALL, "korean");
	SQLHENV henv;
	SQLHDBC hdbc;
	SQLHSTMT hstmt = 0;

	// Allocate environment handle  
	retcode = SQLAllocHandle(SQL_HANDLE_ENV, SQL_NULL_HANDLE, &henv);

	// Set the ODBC version environment attribute  
	if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {
		retcode = SQLSetEnvAttr(henv, SQL_ATTR_ODBC_VERSION, (SQLPOINTER*)SQL_OV_ODBC3, 0);

		// Allocate connection handle  
		if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {
			retcode = SQLAllocHandle(SQL_HANDLE_DBC, henv, &hdbc);

			// Set login timeout to 5 seconds  
			if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {
				SQLSetConnectAttr(hdbc, SQL_LOGIN_TIMEOUT, (SQLPOINTER)5, 0);

				// Connect to data source  
				retcode = SQLConnect(hdbc, (SQLWCHAR*)L"2015182040_CJR_PROJECT", SQL_NTS, (SQLWCHAR*)NULL, SQL_NTS, NULL, SQL_NTS);

				// Allocate statement handle  
				if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {
					retcode = SQLAllocHandle(SQL_HANDLE_STMT, hdbc, &hstmt);		// 명령어 받을 핸들 할당. hstmt.

					SQLWCHAR query[1024];
					wsprintf(query, L"INSERT INTO User_Table values(%d, %d, %d, %d, %d, %d)", id, x, y, 1, 100, 0);
					retcode = SQLExecDirect(hstmt, (SQLWCHAR *)query, SQL_NTS);
					if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {
						printf("DB 새로운 데이터 생성 성공");
					}
					else {
						HandleDiagnosticRecord(hstmt, SQL_HANDLE_STMT, retcode);
					}

					// Process data  
					if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {
						SQLCancel(hstmt);
						SQLFreeHandle(SQL_HANDLE_STMT, hstmt);
					}

					SQLDisconnect(hdbc);
				}

				SQLFreeHandle(SQL_HANDLE_DBC, hdbc);
			}
		}
		SQLFreeHandle(SQL_HANDLE_ENV, henv);
	}
}

void error_display(const char *msg, int err_no)
{
	WCHAR *lpMsgBuf;
	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER |
		FORMAT_MESSAGE_FROM_SYSTEM,
		NULL, err_no,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&lpMsgBuf, 0, NULL);
	//cout << msg;
	//wcout << L"에러 " << lpMsgBuf << endl;
	//while (true);
	LocalFree(lpMsgBuf);
}

void lua_error(lua_State* L, const char* fmt, ...) {
	va_list argp;
	va_start(argp, fmt);
	vfprintf(stderr, fmt, argp);
	va_end(argp);
	lua_close(L);
	while (true);
	exit(EXIT_FAILURE);
}

void add_timer(EVENT &ev)
{
	timer_lock.lock();
	timer_queue.push(ev);
	timer_lock.unlock();
}

bool Is_NPC(int id)
{
	return id >= NPC_ID_START;
}

bool Is_Active(int npc_id)
{
	return clients[npc_id]->is_active;
}

bool is_near(int a, int b)
{
	if (VIEW_RANGE < abs(clients[a]->x - clients[b]->x)) return false;
	if (VIEW_RANGE < abs(clients[a]->y - clients[b]->y)) return false;
	return true;
}

bool is_near_NPC(int a, int b)
{
	if (VIEW_RANGE + VIEW_RANGE < abs(clients[a]->x - clients[b]->x)) return false;
	if (VIEW_RANGE + VIEW_RANGE < abs(clients[a]->y - clients[b]->y)) return false;
	return true;
}

void send_packet(int id, void *buff)
{
	char *packet = reinterpret_cast<char *>(buff);
	int packet_size = packet[0];
	OVER_EX *send_over = new OVER_EX;
	memset(send_over, 0x00, sizeof(OVER_EX));
	send_over->event_type = EV_SEND;
	memcpy(send_over->net_buf, packet, packet_size);
	send_over->wsabuf[0].buf = send_over->net_buf;
	send_over->wsabuf[0].len = packet_size;
	int ret = WSASend(clients[id]->socket, send_over->wsabuf, 1, 0, 0, &send_over->over, 0);
	if (0 != ret) {
		int err_no = WSAGetLastError();
		if (WSA_IO_PENDING != err_no)
			error_display("WSASend Error :", err_no);
	}
}

void send_login_ok_packet(int id)
{
	sc_packet_login_ok packet;
	packet.id = id;
	packet.size = sizeof(packet);
	packet.type = SC_LOGIN_OK;
	send_packet(id, &packet);
}

void send_put_object_packet(int client, int new_id)
{
	sc_packet_put_object packet;
	packet.id = new_id;
	packet.level = clients[new_id]->level;
	packet.size = sizeof(packet);
	packet.type = SC_PUT_OBJECT;
	packet.x = clients[new_id]->x;
	packet.y = clients[new_id]->y;
	send_packet(client, &packet);

	if (client == new_id) return;
	lock_guard<mutex>lg{ clients[client]->near_lock };
	clients[client]->near_id.insert(new_id);
}

void send_pos_packet(int client, int mover)
{
	sc_packet_pos packet;
	packet.id = mover;
	packet.size = sizeof(packet);
	packet.type = SC_POS;
	packet.x = clients[mover]->x;
	packet.y = clients[mover]->y;

	clients[client]->near_lock.lock();
	if (0 != clients[client]->near_id.count(mover)) {
		clients[client]->near_lock.unlock();
		send_packet(client, &packet);
	}
	else {
		clients[client]->near_lock.unlock();
		send_put_object_packet(client, mover);
	}
}

void send_remove_object_packet(int client, int leaver)
{
	sc_packet_remove_object packet;
	packet.id = leaver;
	packet.size = sizeof(packet);
	packet.type = SC_REMOVE_OBJECT;
	send_packet(client, &packet);

	lock_guard<mutex>lg{ clients[client]->near_lock };
	clients[client]->near_id.erase(leaver);
}

void send_chat_player_packet(int to, char* packet)
{
	packet[1] = SC_CHAT;
	send_packet(to, packet);
}

void send_chat_packet(int client, int chatter, char mess[])
{
	sc_packet_chat packet;
	packet.id = chatter;
	packet.size = sizeof(packet);
	packet.type = SC_CHAT;
	strcpy_s(packet.chat, mess);
	send_packet(client, &packet);
}

void send_stat_change_packet(int to, int who)
{
	sc_packet_stat_change packet;
	packet.type = SC_STAT_CHANGE;
	packet.c_exp = clients[who]->c_exp;
	packet.m_exp = clients[who]->m_exp;
	packet.c_hp = clients[who]->c_hp;
	packet.m_hp = clients[who]->m_hp;
	packet.level = clients[who]->level;
	packet.id = who;
	packet.size = sizeof(packet);

	send_packet(to, &packet);
}

bool is_near_id(int player, int other)
{
	lock_guard <mutex> gl{ clients[player]->near_lock };
	return (0 != clients[player]->near_id.count(other));
}

void update_player_inform(int player, int npc)
{
	clients[npc]->is_connected = false;
	for (auto &cl : clients) {
		if (false == Is_NPC(cl.first))
			send_remove_object_packet(cl.first, npc);
	}

	clients[player]->c_exp += clients[npc]->level * clients[npc]->level * 5;

	while (clients[player]->c_exp >= clients[player]->m_exp) {
		clients[player]->c_exp -= clients[player]->m_exp;

		clients[player]->level++;
		clients[player]->damage = 35 + clients[player]->level * 5;
		clients[player]->m_hp = 100 + (20 * (clients[player]->level - 1));
		clients[player]->c_hp = clients[player]->m_hp;
		clients[player]->m_exp = 100 + (100 * (clients[player]->level - 1));
	}
	send_stat_change_packet(player, player);
	for (auto& cl : clients[player]->near_id) {
		if (true == Is_NPC(cl)) continue;
		send_stat_change_packet(cl, player);
	}
	EVENT new_ev{ npc, high_resolution_clock::now() + 30s, EV_ALIVE, 0 };
	add_timer(new_ev);
}

void send_monster_inform(int player, int npc_id)
{
	sc_packet_monster_stat packet;
	packet.size = sizeof(packet);
	packet.type = SC_MONSTER_STAT;
	packet.id = npc_id;
	packet.c_hp = clients[npc_id]->c_hp;

	send_packet(player, &packet);
}

void ProcessPacket(int id, void *buff)
{
	char *packet = reinterpret_cast<char *>(buff);
	short x = clients[id]->x;
	short y = clients[id]->y;
	clients[id]->near_lock.lock();
	auto old_vl = clients[id]->near_id;
	clients[id]->near_lock.unlock();
	switch (packet[1]) {
	case CS_MOVE:
		if (true == clients[id]->is_move) {
			clients[id]->is_move = false;
			switch (packet[2]) {
			case UP: if (y > 0) y--;
				break;
			case DOWN: if (y < WORLD_HEIGHT - 1) y++;
				break;
			case LEFT: if (x > 0) x--;
				break;
			case RIGHT: if (x < WORLD_WIDTH - 1) x++;
				break;
			default: cout << "Invalid Packet Type Error\n";
				//while (true);
			}
			if (g_map[x][y] == '0') break;
			clients[id]->x = x;
			clients[id]->y = y;
			EVENT new_ev{ id, high_resolution_clock::now() + 500ms, EV_PLAYER_MOVE, 0 };
			add_timer(new_ev);
		}
		break;
	case CS_ATTACK:
		if (true == clients[id]->is_attack) {
			clients[id]->is_attack = false;
			for (auto& cl : old_vl) {
				if (false == Is_NPC(cl)) continue;
				if (false == clients[cl]->is_connected) continue;

				if (((abs(clients[id]->x - clients[cl]->x) < 2) && (clients[id]->y == clients[cl]->y)) ||
					((abs(clients[id]->y - clients[cl]->y) < 2) && (clients[id]->x == clients[cl]->x))) {
					cout << id << " Player Attack " << cl << " Monster!!! Damage : " << clients[id]->damage << endl;
					clients[cl]->is_damage = true;
					EVENT new_ev{ cl, high_resolution_clock::now() + 2s, EV_DAMAGE, 0 };
					add_timer(new_ev);
					clients[cl]->c_hp -= clients[id]->damage;
					send_monster_inform(id, cl);
					//send_monster_stat_change_packet(cl);
					if (0 >= clients[cl]->c_hp) {
						update_player_inform(id, cl);
					}
				}
			}
			EVENT new_ev{ id, high_resolution_clock::now() + 1s, EV_ATTACK, 0 };
			add_timer(new_ev);
		}
		break;

	case CS_CHAT: {
		cs_packet_chat* p = reinterpret_cast<cs_packet_chat*>(packet);

		struct tm *date;
		const time_t t = time(NULL);
		date = localtime(&t);

		// 구조체에 채팅 정보 담기
		CHAT c;
		c.id = p->id;
		c.month = date->tm_mon + 1;
		c.day = date->tm_mday;
		c.hour = date->tm_hour;
		c.min = date->tm_min;
		c.sec = date->tm_sec;
		strcpy_s(c.chat, p->chat_str);

		g_dchat.push_back(c);	// 큐에 넣기

		// 나 포함 주변 플레이어에게만 전송
		send_chat_player_packet(id, packet);
		for (auto& cl : clients[id]->near_id) {
			if (true == Is_NPC(cl)) continue;
			if (false == clients[cl]->is_connected) continue;
			send_chat_player_packet(cl, packet);
		}
		break;
	}

	case CS_TELEPORT: 
		switch (packet[2]) {
		case A:
			clients[id]->x = 100;
			clients[id]->y = 100;
			break;
		case B:
			clients[id]->x = 300;
			clients[id]->y = 300;
			break;
		case C:
			clients[id]->x = 500;
			clients[id]->y = 500;
			break;
		case D:
			clients[id]->x = 700;
			clients[id]->y = 700;
			break;
		}
		break;
	case CS_LOGOUT:
		SavePos_DB(clients[id]->x, clients[id]->y, clients[id]->level, clients[id]->c_hp, clients[id]->c_exp, clients[id]->id);
		cout << "클라이언트 종료: " << id << endl;
		clients[id]->is_connected = false;
		closesocket(clients[id]->socket);
		for (auto &cl : clients) {
			if (false == Is_NPC(cl.first))
				send_remove_object_packet(cl.first, id);
		}
		break;
	default: cout << "Invalid Packet Type Error\n";
		while (true);
	}

	set <int> new_vl;
	for (auto &cl : clients) {
		int other = cl.second->id;
		if (id == other) continue;
		if (true == is_near_NPC(id, other)) {
			if (true == Is_NPC(other) && (false == Is_Active(other))) {
				clients[other]->is_active = true;
				if (LEVEL3 == clients[other]->level || LEVEL4 == clients[other]->level) {
					clients[other]->target = id;
					EVENT ev{ other, high_resolution_clock::now() + 1s, EV_MOVE_TARGET, id };
					add_timer(ev);
				}
				else {
					EVENT ev{ other, high_resolution_clock::now() + 1s, EV_MOVE, 0 };
					add_timer(ev);
				}
			}
			if (true == is_near(id, other)) {
				new_vl.insert(other);
				if (true == Is_NPC(other)) {
					OVER_EX *over_ex = new OVER_EX;
					over_ex->event_type = EV_PLAYER_MOVE_NOTIFY;
					*(int *)(over_ex->net_buf) = id;
					PostQueuedCompletionStatus(g_iocp, 1, other, &over_ex->over);
				}
				else {
					send_stat_change_packet(id, other);
				}
			}
		}
	}

	send_pos_packet(id, id);
	for (auto cl : old_vl) {
		if (0 != new_vl.count(cl)) {
			if (false == Is_NPC(cl))
				send_pos_packet(cl, id);
		}
		else {
			send_remove_object_packet(id, cl);
			if (false == Is_NPC(cl)) {
				send_remove_object_packet(cl, id);
			}
		}
	}
	for (auto cl : new_vl) {
		if (0 == old_vl.count(cl) &&
			true == clients[cl]->is_connected) {
			send_put_object_packet(id, cl);
			send_stat_change_packet(id, cl);
			if (false == Is_NPC(cl) &&
				true == clients[cl]->is_connected) {
				send_put_object_packet(cl, id);
				send_stat_change_packet(cl, id);
			}
			else 
				send_put_object_packet(id, cl);
		}
	}
}

void Init_NPC(int npc_id)
{
	switch (clients[npc_id]->type) {
	case LEVEL1:
		clients[npc_id]->c_hp = 50;
		clients[npc_id]->m_hp = 50;
		clients[npc_id]->damage = 3;
		clients[npc_id]->level = 1;
		clients[npc_id]->c_exp = 0;
		clients[npc_id]->m_exp = 0;
		break;
	case LEVEL2:
		clients[npc_id]->c_hp = 100;
		clients[npc_id]->m_hp = 150;
		clients[npc_id]->damage = 5;
		clients[npc_id]->level = 3;
		clients[npc_id]->c_exp = 0;
		clients[npc_id]->m_exp = 0;
		break;
	case LEVEL3:
		clients[npc_id]->c_hp = 150;
		clients[npc_id]->m_hp = 150;
		clients[npc_id]->damage = 10;
		clients[npc_id]->level = 5;
		clients[npc_id]->c_exp = 0;
		clients[npc_id]->m_exp = 0;
		break;
	case LEVEL4:
		clients[npc_id]->c_hp = 300;
		clients[npc_id]->m_hp = 300;
		clients[npc_id]->damage = 20;
		clients[npc_id]->level = 7;
		clients[npc_id]->c_exp = 0;
		clients[npc_id]->m_exp = 0;
		break;
	}
}

void do_target_move(int player, int npc_id)
{
	if (false == clients[npc_id]->is_connected) return;

	if (0 == clients.count(npc_id)) {
		cout << "NPC: " << npc_id << " does not EXIST!\n";
		while (true);
	}

	if (false == Is_NPC(npc_id)) {
		cout << "ID :" << npc_id << " is not NPC!!\n";
		while (true);
	}

	//if (0 == clients[npc_id]->target) return;

	bool player_exists = false;
	for (int i = 0; i < NPC_ID_START; i++) {
		if (0 == clients.count(i)) continue;
		if (true == is_near_NPC(i, npc_id)) {
			player_exists = true;
			if (false == clients[npc_id]->is_active) {
				clients[npc_id]->is_active = true;
			}
			break;
		}
	}
	if (false == player_exists) {
		clients[npc_id]->is_active = false;
		Init_NPC(npc_id);
		return;
	}

	SOCKETINFO *npc = clients[npc_id];
	int x = npc->x;
	int y = npc->y;
	set <int> old_view_list;
	for (auto &obj : clients) {
		if (true == is_near(npc->id, obj.second->id) &&
			true == npc->is_connected &&
			true == obj.second->is_connected)
			old_view_list.insert(obj.second->id);
	}

	int target_x = npc->x - clients[player]->x;
	int target_y = npc->y - clients[player]->y;
	int move_dir;

	if (target_x == 0 && target_y == 0) return;

	while (true) {
		move_dir = rand() % 2;
		switch (move_dir) {
		case 0:
			if (0 == target_x) continue;
			if (target_x < 0) ++x;
			else --x;
			break;
		case 1:
			if (0 == target_y) continue;
			if (target_y < 0) ++y;
			else --y;
			break;
		}
		if (g_map[x][y] == '0') continue;
		if (npc->is_damage == false) {
			npc->x = x;
			npc->y = y;
		}
		break;
	}

	set <int> new_view_list;
	for (auto &obj : clients) {
		if (true == is_near(npc->id, obj.second->id) &&
			true == npc->is_connected &&
			true == obj.second->is_connected)
			new_view_list.insert(obj.second->id);
	}
	for (auto &pc : clients) {
		if (true == Is_NPC(pc.second->id)) continue;
		if (false == pc.second->is_connected) continue;
		if (false == npc->is_active) continue;
		if (false == is_near(pc.second->id, npc->id)) continue;
		send_pos_packet(pc.second->id, npc->id);
	}

	EVENT new_ev{ npc_id, high_resolution_clock::now() + 1s, EV_MOVE_TARGET, player };
	add_timer(new_ev);
}

void do_random_move(int npc_id)
{
	if (false == clients[npc_id]->is_connected) return;

	if (0 == clients.count(npc_id)) {
		cout << "NPC: " << npc_id << " does not EXIST!\n";
		while (true);
	}

	if (false == Is_NPC(npc_id)) {
		cout << "ID :" << npc_id << " is not NPC!!\n";
		while (true);
	}

	bool player_exists = false;
	for (int i = 0; i < NPC_ID_START; i++) {
		if (0 == clients.count(i)) continue;
		if (true == is_near_NPC(i, npc_id)) {
			player_exists = true;
			if (false == clients[npc_id]->is_active) {
				clients[npc_id]->is_active = true;
			}
			break;
		}
	}
	if (false == player_exists) {
		clients[npc_id]->is_active = false;
		Init_NPC(npc_id);
		return;
	}

	SOCKETINFO *npc = clients[npc_id];
	int x = npc->x;
	int y = npc->y;
	set <int> old_view_list;
	for (auto &obj : clients) {
		if (true == is_near(npc->id, obj.second->id) &&
			true == npc->is_connected &&
			true == obj.second->is_connected)
			old_view_list.insert(obj.second->id);
	}
	while (true) {
		switch (rand() % 4) {
		case 0: if (y > 0) y--; break;
		case 1: if (y < (WORLD_HEIGHT - 1)) y++; break;
		case 2: if (x > 0) x--; break;
		case 3: if (x < (WORLD_WIDTH - 1)) x++; break;
		}
		if (g_map[x][y] == '0') continue;
		if (npc->is_damage == false) {
			npc->x = x;
			npc->y = y;
		}
		break;
	}
	set <int> new_view_list;
	for (auto &obj : clients) {
		if (true == is_near(npc->id, obj.second->id) &&
			true == npc->is_connected &&
			true == obj.second->is_connected)
			new_view_list.insert(obj.second->id);
	}
	for (auto &pc : clients) {
		if (true == Is_NPC(pc.second->id)) continue;
		if (false == pc.second->is_connected) continue;
		if (false == npc->is_active) continue;
		if (false == is_near(pc.second->id, npc->id)) continue;
		send_pos_packet(pc.second->id, npc->id);
	}

	EVENT new_ev{ npc_id, high_resolution_clock::now() + 1s, EV_MOVE, 0 };
	add_timer(new_ev);
}

void heal_object(int id)
{
	if (clients[id]->c_hp < clients[id]->m_hp) {
		short amount = clients[id]->m_hp * 0.05;
		short new_hp = clients[id]->c_hp + amount;

		if (new_hp < clients[id]->m_hp) {
			clients[id]->c_hp = new_hp;
		}
		else {
			clients[id]->c_hp = clients[id]->m_hp;
		}
	}
	EVENT ev{ id, high_resolution_clock::now() + 5s, EV_HEAL, 0 };
	add_timer(ev);
}

void save_chat()
{
	out << "----------------------------" << std::endl;
	int c;
	while (false == g_dchat.empty()) {
		CHAT c = g_dchat.front();
		g_dchat.pop_front();
		out << "ID: " << c.id << " / " 
			<< c.month << "월 " << c.day << "일 " 
			<< c.hour << "시 " << c.min << "분 " << c.sec << "초" 
			<< std::endl;
		out << c.chat << std::endl;
	}
	out << "----------------------------" << std::endl;
}

void do_worker()
{
	while (true) {
		DWORD num_byte;
		ULONGLONG key64;
		PULONG_PTR p_key = &key64;
		WSAOVERLAPPED *p_over;

		GetQueuedCompletionStatus(g_iocp, &num_byte, p_key, &p_over, INFINITE);
		unsigned int key = static_cast<unsigned>(key64);
		SOCKET client_s;
		if (-1 != key) client_s = clients[key]->socket;
			if (num_byte == 0) {
				//SavePos_DB(clients[key]->x, clients[key]->y, clients[key]->level, clients[key]->c_hp, clients[key]->c_exp, clients[key]->id);
				cout << "클라이언트 종료: " << key << endl;
				if (clients[key]->is_connected != false) {
					clients[key]->is_connected = false;
					closesocket(client_s);
					for (auto &cl : clients) {
						if (false == Is_NPC(cl.first))
							send_remove_object_packet(cl.first, key);
					}
				}
				continue;
			}  // 클라이언트가 closesocket을 했을 경우		
		OVER_EX *over_ex = reinterpret_cast<OVER_EX *> (p_over);

		if (EV_RECV == over_ex->event_type) {
			ProcessPacket(key, over_ex->net_buf);

			DWORD flags = 0;
			memset(&over_ex->over, 0x00, sizeof(WSAOVERLAPPED));
			WSARecv(client_s, over_ex->wsabuf, 1, 0, &flags, &over_ex->over, 0);
		}
		else if (EV_SEND == over_ex->event_type) {
			delete over_ex;
		}
		else if (EV_MOVE == over_ex->event_type) {
			do_random_move(key);
			delete over_ex;
		}
		else if (EV_MOVE_TARGET == over_ex->event_type) {
			int player_id = *(int *)(over_ex->net_buf);
			do_target_move(player_id, key);
			delete over_ex;
		}
		else if (EV_PLAYER_MOVE == over_ex->event_type) {
			clients[key]->is_move = true;
			delete over_ex;
		}
		else if (EV_PLAYER_MOVE_NOTIFY == over_ex->event_type) {
			int player_id = *(int *)(over_ex->net_buf);
			//cout << "MOVE EVENT from: " << player_id << "  TO: " << key << endl;
			lua_State *L = clients[key]->L;
			clients[key]->vm_lock.lock();
			lua_getglobal(L, "event_player_attack");
			lua_pushnumber(L, player_id);
			lua_pushnumber(L, clients[player_id]->x);
			lua_pushnumber(L, clients[player_id]->y);
			if (0 != lua_pcall(L, 3, 0, 0))
				lua_error(L, "error running monster.lua : %s\n",
					lua_tostring(L, -1));
			//lua_pcall(L, 3, 0, 0);
			clients[key]->vm_lock.unlock();
			delete over_ex;
		}
		else if (EV_ATTACK == over_ex->event_type) {
			clients[key]->is_attack = true;
			delete over_ex;
		}
		else if (EV_DAMAGE == over_ex->event_type) {
			clients[key]->is_damage = false;
			delete over_ex;
		}
		else if (EV_HEAL == over_ex->event_type) {
			heal_object(key);
			send_stat_change_packet(key, key);
			for (auto& cl : clients[key]->near_id) {
				if (true == Is_NPC(cl)) continue;
				send_stat_change_packet(cl, key);
			}
			delete over_ex;
		}
		else if (EV_ALIVE == over_ex->event_type) {
			Init_NPC(key);
			clients[key]->is_connected = true;
			delete over_ex;
		}
		else if (EV_CHAT == over_ex->event_type) {
			save_chat();
			delete over_ex;
		}
		else {
			cout << "Unknown Event Type :" << over_ex->event_type << endl;
			while (true);
		}
	}
}

int lua_get_x_position(lua_State *L)
{
	int npc_id = (int)lua_tonumber(L, -1);
	lua_pop(L, 2);
	int x = clients[npc_id]->x;
	lua_pushnumber(L, x);
	return 1;
}

int lua_get_y_position(lua_State *L)
{
	int npc_id = (int)lua_tonumber(L, -1);
	lua_pop(L, 2);
	int y = clients[npc_id]->y;
	lua_pushnumber(L, y);
	return 1;
}

int lua_send_chat_packet(lua_State *L)
{
	int player_id = (int)lua_tonumber(L, -3);
	int chatter_id = (int)lua_tonumber(L, -2);
	char *mess = (char *)lua_tostring(L, -1);
	lua_pop(L, 4);
	cout << "CHAT : from= " << chatter_id << ",  to= " << player_id << ", [" << mess << "]\n";
	send_chat_packet(player_id, chatter_id, mess);
	return 0;
}

void update_npc_inform(int npc, int player)
{
	clients[player]->c_hp -= clients[npc]->damage;

	if (0 >= clients[player]->c_hp) {
		clients[player]->x = 100;
		clients[player]->y = 100;
		clients[player]->c_hp = clients[player]->m_hp;
		if (0 < clients[player]->c_exp) 
		clients[player]->c_exp = clients[player]->c_exp / 2;
	}

	send_stat_change_packet(player, player);
	for (auto& cl : clients[player]->near_id) {
		if (true == Is_NPC(cl)) continue;
		send_stat_change_packet(cl, player);
	}
}

int lua_npc_attack(lua_State* L)
{
	int player = (int)lua_tonumber(L, -2);
	int npc_id = (int)lua_tonumber(L, -1);
	lua_pop(L, 3);
	cout << "Attack" << endl;

	if (true == clients[npc_id]->is_attack && 
		false == clients[npc_id]->is_damage &&
		true == clients[npc_id]->is_connected) {
		clients[npc_id]->is_attack = false;

		update_npc_inform(npc_id, player);

		EVENT ev{ npc_id, high_resolution_clock::now() + 1s, EV_ATTACK, 0 };
		add_timer(ev);
	}
	return 0;
}

void Create_NPC()
{
	cout << "Initializing NPC\n";
	for (int npc_id = NPC_ID_START; npc_id < NPC_ID_START + NUM_NPC; ++npc_id) {
		clients[npc_id] = new SOCKETINFO;
		clients[npc_id]->id = npc_id;
		clients[npc_id]->is_connected = true;
		clients[npc_id]->is_attack = true;
		clients[npc_id]->is_active = false;
		clients[npc_id]->is_damage = false;

		while (true) {
			int x = rand() % WORLD_WIDTH;
			int y = rand() % WORLD_HEIGHT;
			
			if (g_map[x][y] == '0') continue;
			
			clients[npc_id]->x = x;
			clients[npc_id]->y = y;
			break;
		}

		int y = (int)(clients[npc_id]->y / 200.f);
		switch (y) {
		case 0:
			clients[npc_id]->level = LEVEL1;
			break;
		case 1:
			clients[npc_id]->level = LEVEL2;
			break;
		case 2:
			clients[npc_id]->level = LEVEL3;
			break;
		default:
			clients[npc_id]->level = LEVEL4;
			break;
		}

		switch (clients[npc_id]->level) {
		case LEVEL1:
			clients[npc_id]->c_hp = 50;
			clients[npc_id]->m_hp = 50;
			clients[npc_id]->damage = 3;
			clients[npc_id]->c_exp = 0;
			clients[npc_id]->m_exp = 0;
			break;
		case LEVEL2:
			clients[npc_id]->c_hp = 100;
			clients[npc_id]->m_hp = 150;
			clients[npc_id]->damage = 5;
			clients[npc_id]->c_exp = 0;
			clients[npc_id]->m_exp = 0;
			break;
		case LEVEL3:
			clients[npc_id]->c_hp = 150;
			clients[npc_id]->m_hp = 150;
			clients[npc_id]->damage = 10;
			clients[npc_id]->c_exp = 0;
			clients[npc_id]->m_exp = 0;
			break;
		case LEVEL4:
			clients[npc_id]->c_hp = 300;
			clients[npc_id]->m_hp = 300;
			clients[npc_id]->damage = 20;
			clients[npc_id]->c_exp = 0;
			clients[npc_id]->m_exp = 0;
			break;
		}
		clients[npc_id]->socket = -1;
		lua_State *L = clients[npc_id]->L = luaL_newstate();
		luaL_openlibs(L);
		luaL_loadfile(L, "monster.lua");
		lua_pcall(L, 0, 0, 0);
		lua_getglobal(L, "set_npc_id");
		lua_pushnumber(L, npc_id);
		lua_pcall(L, 1, 0, 0);
		lua_register(L, "API_get_x_position", lua_get_x_position);
		lua_register(L, "API_get_y_position", lua_get_y_position);
		//lua_register(L, "API_send_chat_packet", lua_send_chat_packet);
		lua_register(L, "API_npc_attack", lua_npc_attack);
	}
	cout << "NPC Initialization finished.\n";
}

void do_timer()
{
	while (true) {
		timer_lock.lock();
		while (true == timer_queue.empty()) {
			timer_lock.unlock();
			this_thread::sleep_for(10ms);
			timer_lock.lock();
		}
		const EVENT& ev = timer_queue.top();
		if (ev.wakeup_time > high_resolution_clock::now()) {
			timer_lock.unlock();
			this_thread::sleep_for(10ms);
				continue;
		}
		EVENT p_ev = ev;
		//cout << "ID : " << p_ev.obj_id << ", EVENT : " << p_ev.event_type << endl;
		timer_queue.pop();
		timer_lock.unlock();

		OVER_EX* over_ex = new OVER_EX;
		over_ex->event_type = p_ev.event_type;
		*(int*)(over_ex->net_buf) = p_ev.target_obj;
		PostQueuedCompletionStatus(g_iocp, 1, p_ev.obj_id, &over_ex->over);
	}
}

void Create_map()
{
	cout << "Create Map" << endl;
	int xIndex = 0;
	int yIndex = 0;
	char data = 0;
	while (!in.eof()) {
		in >> data;
		g_map[xIndex][yIndex] = data;

		yIndex++;
		if (yIndex == 800) {
			xIndex++;
			yIndex = 0;
		}
	}
	cout << "Finish" << endl;
}

int main()
{
	wcout.imbue(std::locale("korean"));

	Create_map();
	Create_NPC();

	WSADATA WSAData;
	WSAStartup(MAKEWORD(2, 2), &WSAData);
	SOCKET listenSocket = WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);
	if (0 != listenSocket) {
		int err_no = WSAGetLastError();
		if (WSA_IO_PENDING != err_no)
			error_display("Listen Socket Error : ", err_no);
	}
	SOCKADDR_IN serverAddr;
	memset(&serverAddr, 0, sizeof(SOCKADDR_IN));
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = htons(SERVER_PORT);
	serverAddr.sin_addr.S_un.S_addr = htonl(INADDR_ANY);
	::bind(listenSocket, (struct sockaddr*)&serverAddr, sizeof(SOCKADDR_IN));
	listen(listenSocket, 5);
	SOCKADDR_IN clientAddr;
	int addrLen = sizeof(SOCKADDR_IN);
	memset(&clientAddr, 0, addrLen);
	SOCKET clientSocket;
	DWORD flags;

	g_iocp = CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, NULL, 0);
	thread worker_thread1{ do_worker };
	thread worker_thread2{ do_worker };
	thread worker_thread3{ do_worker };
	//thread ai_thread{ do_ai };
	thread timer_thread{ do_timer };

	while (true) {
		clientSocket = accept(listenSocket, (struct sockaddr *)&clientAddr, &addrLen);
		// 디비 시작
		cs_packet_login packet;
		ZeroMemory(&packet, sizeof(cs_packet_login));
		recv(clientSocket, (char*)&packet, sizeof(cs_packet_login), 0);
		new_user_id = packet.id;
		cout << new_user_id << endl;
		cout << packet.id << endl;
		Check_ID_DB();
		if (new_user_id >= NPC_ID_START) {
			sc_packet_login_check packet_fail;
			packet_fail.type = SC_LOGIN_FAIL;
			send(clientSocket, (char*)&packet_fail, sizeof(sc_packet_login_check), 0);
			cout << "Login Fail\n";
			closesocket(clientSocket);
			continue;
		}
		if (0 != clients.count(new_user_id)) {
			if (true == clients[new_user_id]->is_connected) {
				sc_packet_login_check packet_fail;
				packet_fail.type = SC_LOGIN_FAIL;
				send(clientSocket, (char*)&packet_fail, sizeof(sc_packet_login_check), 0);
				cout << "Login Fail\n";
				closesocket(clientSocket);
				continue;
			}
		}
		sc_packet_login_check packet_ok;
		packet_ok.type = SC_LOGIN_OK;
		send(clientSocket, (char*)&packet_ok, sizeof(sc_packet_login_check), 0);

		SOCKETINFO *new_player = new SOCKETINFO;
		new_player->id = new_user_id;
		new_player->socket = clientSocket;
		new_player->is_move = true;
		new_player->is_connected = true;
		new_player->is_attack = true;
		new_player->is_damage = false;
		new_player->recv_over.wsabuf[0].len = MAX_BUFFER;
		new_player->recv_over.wsabuf[0].buf = new_player->recv_over.net_buf;
		new_player->recv_over.event_type = EV_RECV;
		bool is_used = false;
		for (const int& id : is_connected) {
			if (id == new_user_id) {
				is_used = true;
				break;
			}
		}
		for (int& id : is_connected) {
			id = 0;
		}
		if (true == is_used) {
			cout << "Login Success\n";
			InitPos_DB(new_user_id);
			new_player->x = nX;
			new_player->y = nY;
			new_player->level = nLEVEL;
			new_player->c_hp = nHP;
			new_player->m_hp = 100 + (20 * (new_player->level - 1));
			new_player->damage = 35 + new_player->level * 5;
			new_player->c_exp = nEXP;
			new_player->m_exp = 100 + (100 * (new_player->level - 1));
			clients.insert(make_pair(new_user_id, new_player));
		}
		else {
			cout << "Make ID\n";
			new_player->x = 100;
			new_player->y = 100;
			new_player->level = 1;
			new_player->c_hp = 100;
			new_player->m_hp = 100;
			new_player->damage = 35;
			new_player->c_exp = 0;
			new_player->m_exp = 100;
			clients.insert(make_pair(new_user_id, new_player));
			Make_ID_DB(new_user_id, new_player->x, new_player->y);
		}

		sc_packet_login_ok packet_login_ok;
		packet_login_ok.type = SC_LOGIN_OK;
		packet_login_ok.size = sizeof(packet_ok);
		packet_login_ok.x = nX;
		packet_login_ok.y = nY;
		packet_login_ok.id = new_user_id;
		packet_login_ok.level = new_player->level;
		packet_login_ok.hp = new_player->c_hp;
		packet_login_ok.exp = new_player->c_exp;
		send(clientSocket, (char*)&packet_login_ok, sizeof(packet_login_ok), 0);

		CreateIoCompletionPort(reinterpret_cast<HANDLE>(clientSocket), g_iocp, new_user_id, 0);

		EVENT ev{ new_user_id, high_resolution_clock::now() + 5s, EV_HEAL, 0 };
		add_timer(ev);

		for (auto &cl : clients) {
			int other_player = cl.first;
			if ((true == Is_NPC(other_player))
				&& (true == is_near_NPC(other_player, new_user_id))
				&& (true == clients[other_player]->is_connected)) {
				clients[new_user_id]->near_id.insert(other_player);
				if (false == Is_Active(other_player)) {
					clients[other_player]->is_active = true;
					EVENT ev{ other_player, high_resolution_clock::now() + 1s, EV_MOVE, 0 };
					add_timer(ev);
				}
			}
			if (false == clients[other_player]->is_connected) continue;
			if (true == is_near(other_player, new_user_id)) {
				if (false == Is_NPC(other_player)) {
					send_stat_change_packet(other_player, new_user_id);
					send_put_object_packet(other_player, new_user_id);
				}
				else {
					send_put_object_packet(new_user_id, other_player);
				}
				if (other_player != new_user_id) {
					send_stat_change_packet(new_user_id, other_player);
					send_put_object_packet(new_user_id, other_player);
				}
			}
		}
		memset(&clients[new_user_id]->recv_over.over, 0, sizeof(clients[new_user_id]->recv_over.over));
		flags = 0;
		int ret = WSARecv(clientSocket, clients[new_user_id]->recv_over.wsabuf, 1, NULL,
			&flags, &(clients[new_user_id]->recv_over.over), NULL);
		if (0 != ret) {
			int err_no = WSAGetLastError();
			if (WSA_IO_PENDING != err_no)
				error_display("WSARecv Error :", err_no);
		}
		// 디비 끝

		// 더미 시작
		//int user_id = new_user_id++;
		//SOCKETINFO *new_player = new SOCKETINFO;
		//new_player->is_connected = true;
		//new_player->is_attack = true;
		//new_player->is_damage = false;
		//new_player->is_move = true;
		//new_player->id = user_id;
		//new_player->socket = clientSocket;
		//new_player->recv_over.wsabuf[0].len = MAX_BUFFER;
		//new_player->recv_over.wsabuf[0].buf = new_player->recv_over.net_buf;
		//new_player->recv_over.event_type = EV_RECV;
		//new_player->x = rand() % WORLD_WIDTH;
		//new_player->y = rand() % WORLD_HEIGHT;
		//new_player->level = 1;
		//new_player->c_hp = 100;
		//new_player->m_hp = 100 + (20 * (new_player->level - 1));
		//new_player->damage = 35 + new_player->level * 5;
		//new_player->c_exp = 0;
		//new_player->m_exp = 100 + (100 * (new_player->level - 1));
		//clients.insert(make_pair(user_id, new_player));
		//CreateIoCompletionPort(reinterpret_cast<HANDLE>(clientSocket), g_iocp, user_id, 0);
		//send_login_ok_packet(user_id);

		//EVENT ev{ user_id, high_resolution_clock::now() + 5s, EV_HEAL, 0 };
		//add_timer(ev);

		//for (auto &cl : clients) {
		//	int other_player = cl.first;
		//	if ((true == Is_NPC(other_player))
		//		&& (true == is_near_NPC(other_player, user_id))
		//		&& (true == clients[other_player]->is_connected)) {
		//		clients[user_id]->near_id.insert(other_player);
		//		
		//		if (false == Is_Active(other_player)) {
		//			clients[other_player]->is_active = true;
		//			EVENT ev{ other_player, high_resolution_clock::now() + 1s, EV_MOVE, 0 };
		//			add_timer(ev);
		//		}
		//	}
		//	if (false == clients[other_player]->is_connected) continue;
		//	if (true == is_near(other_player, user_id)) {
		//		if (false == Is_NPC(other_player)) {
		//			send_stat_change_packet(other_player, user_id);
		//			send_put_object_packet(other_player, user_id);
		//		}
		//		else {
		//			send_put_object_packet(user_id, other_player);
		//		}
		//		if (other_player != user_id) {
		//			send_stat_change_packet(user_id, other_player);
		//			send_put_object_packet(user_id, other_player);
		//		}
		//	}
		//}

		//memset(&clients[user_id]->recv_over.over, 0, sizeof(clients[user_id]->recv_over.over));
		//flags = 0;
		//int ret = WSARecv(clientSocket, clients[user_id]->recv_over.wsabuf, 1, NULL,
		//	&flags, &(clients[user_id]->recv_over.over), NULL);
		//if (0 != ret) {
		//	int err_no = WSAGetLastError();
		//	if (WSA_IO_PENDING != err_no)
		//		error_display("WSARecv Error :", err_no);
		//}
		// 더미 끝

		if (false == g_bchat) {
			EVENT new_ev{ -1, high_resolution_clock::now() + 10s, EV_CHAT, 0 };
			add_timer(new_ev);
		}
	}
	worker_thread1.join();
	worker_thread2.join();
	worker_thread3.join();
	//ai_thread.join();
	timer_thread.join();
	closesocket(listenSocket);
	WSACleanup();
}