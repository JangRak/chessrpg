#pragma once

constexpr int MAX_STR_LEN = 50;

#define WORLD_WIDTH		800
#define WORLD_HEIGHT	800

#define NPC_ID_START	20000

#define SERVER_PORT		3500
#define NUM_NPC			20000

#define NUM_OBSTACLE	10000

#define CS_LOGIN	1
#define CS_MOVE		2
#define CS_ATTACK	3
#define CS_CHAT		4
#define CS_LOGOUT	5
#define CS_TELEPORT 6

#define SC_LOGIN_OK				1
#define SC_LOGIN_FAIL			2
#define SC_POS					3
#define SC_PUT_OBJECT			4
#define SC_REMOVE_OBJECT		5
#define SC_CHAT					6
#define SC_STAT_CHANGE			7
#define SC_MONSTER_STAT			8

enum MOVE { UP, DOWN, LEFT, RIGHT };
enum SITE { A, B, C, D };

#pragma pack(push ,1)

struct sc_packet_login_ok {
	char size;
	char type;
	int id;
	short x, y;
	short hp;
	short level;
	int	exp;
};

struct sc_packet_login_check {
	char size;
	char type;
};

struct sc_packet_pos {
	char size;
	char type;
	int id;
	short x, y;
};

struct sc_packet_put_object {
	char size;
	char type;
	int id;
	int level;
	short x, y;
	// 렌더링 정보, 종족, 성별, 착용 아이템, 캐릭터 외형, 이름, 길드....
};

struct sc_packet_remove_object {
	char size;
	char type;
	int id;
};

struct sc_packet_chat {
	char size;
	char type;
	int	 id;
	char chat[50];
};

struct sc_packet_stat_change {
	char size;
	char type;
	short m_hp;
	short c_hp;
	short level;
	int   m_exp;
	int	  c_exp;
	int		id;
};

struct sc_packet_monster_stat {
	char size;
	char type;
	short c_hp;
	int id;
};

struct cs_packet_login {
	char	size;
	char	type;
	int		id;
};

struct cs_packet_move {
	char	size;
	char	type;
	char	direction;
};

struct cs_packet_attack {
	char	size;
	char	type;
};

struct cs_packet_chat {
	char	size;
	char	type;
	int		id;
	char	chat_str[50];
};

struct cs_packet_logout {
	char	size;
	char	type;
};

struct cs_packet_teleport {
	char	size;
	char	type;
	int		site;
};

#pragma pack (pop)