#define SFML_STATIC 1
#include <SFML/Graphics.hpp>
#include <SFML/Network.hpp>
#include <iostream>
#include <unordered_map>
#include <chrono>
#include <fstream>
using namespace std;
using namespace chrono;

#ifdef _DEBUG
#pragma comment (lib, "lib/sfml-graphics-s-d.lib")
#pragma comment (lib, "lib/sfml-window-s-d.lib")
#pragma comment (lib, "lib/sfml-system-s-d.lib")
#pragma comment (lib, "lib/sfml-network-s-d.lib")
#else
#pragma comment (lib, "lib/sfml-graphics-s.lib")
#pragma comment (lib, "lib/sfml-window-s.lib")
#pragma comment (lib, "lib/sfml-system-s.lib")
#pragma comment (lib, "lib/sfml-network-s.lib")
#endif
#pragma comment (lib, "freetype.lib")
#pragma comment (lib, "opengl32.lib")
#pragma comment (lib, "winmm.lib")
#pragma comment (lib, "ws2_32.lib")

#include "..\..\2019_IOCP\2019_IOCP\protocol.h"

sf::TcpSocket socket;

constexpr auto SCREEN_WIDTH = 20;
constexpr auto SCREEN_HEIGHT = 20;

constexpr auto TILE_WIDTH = 40;
constexpr auto WINDOW_WIDTH = TILE_WIDTH * SCREEN_WIDTH + 10 ;   // size of window
constexpr auto WINDOW_HEIGHT = TILE_WIDTH * SCREEN_WIDTH + 10;
constexpr auto BUF_SIZE = 200;
constexpr auto MAX_USER = 10;

enum MONSTER_TYPE { LEVEL1 = 1, LEVEL2 = 3, LEVEL3 = 5, LEVEL4 = 7, NEUTRAL = 0 };
enum CHAT { NONE, ING, SEND };

int g_left_x;
int g_top_y;
int g_myid;
char g_chatBuff[MAX_STR_LEN];
int g_isChat = NONE;
int g_chatIndex = 0;

sf::RenderWindow *g_window;
sf::Font g_font;

char g_map[WORLD_WIDTH][WORLD_HEIGHT];
ifstream in{ "map.txt" };
int g_tmpid;
bool g_login = false;

class OBJECT {
private:
	bool m_showing;
	sf::Sprite m_sprite;
	char m_mess[MAX_STR_LEN];
	high_resolution_clock::time_point m_time_out;
	sf::Text m_text;
	sf::Text m_text2;

public:
	int m_x, m_y;
	short m_hp;
	short c_hp;
	short m_level;
	int m_exp;
	int c_exp;
	bool is_alive = true;
	OBJECT(sf::Texture &t, int x, int y, int x2, int y2, short _m_hp, short _c_hp, short _level, int _m_exp, int _c_exp) {
		m_showing = false;
		m_sprite.setTexture(t);
		m_sprite.setTextureRect(sf::IntRect(x, y, x2, y2));
		m_time_out = high_resolution_clock::now();

		m_hp = _m_hp;
		c_hp = _c_hp;
		m_level = _level;
		m_exp = _m_exp;
		c_exp = _c_exp;
	}
	OBJECT() {
		m_showing = false;
		m_time_out = high_resolution_clock::now();
	}
	void show()
	{
		m_showing = true;
	}
	void hide()
	{
		m_showing = false;
	}

	void a_move(int x, int y) {
		m_sprite.setPosition((float)x, (float)y);
	}

	void a_draw() {
		g_window->draw(m_sprite);
	}

	void move(int x, int y) {
		m_x = x;
		m_y = y;
	}
	void draw() {
		if (false == m_showing) return;
		float rx = (m_x - g_left_x) * 40.0f + 8;
		float ry = (m_y - g_top_y) * 40.0f + 8;
		m_sprite.setPosition(rx, ry);
		g_window->draw(m_sprite);
		if (high_resolution_clock::now() < m_time_out) {
			m_text.setPosition(rx - 10, ry - 10);
			g_window->draw(m_text);
			g_window->draw(m_text2);
		}

		//if (c_hp > 0) {
		//	sf::Text text_hp;
		//	text_hp.setFont(g_font);
		//	char buf[100];
		//	sprintf_s(buf, "%d", c_hp);
		//	text_hp.setPosition(rx + 5.f, ry - 30.f);
		//	text_hp.setString(buf);
		//	g_window->draw(text_hp);
		//}

	}
	void add_chat(char chat[]) {
		m_text.setFont(g_font);
		m_text.setString(chat);
		m_time_out = high_resolution_clock::now() + 2s;
	}
	void display_hp(int hp) {
		m_text.setFont(g_font);
		char buf[10];
		sprintf_s(buf, "%d", hp);
		m_text.setString(buf);
		m_time_out = high_resolution_clock::now() + 2s;

		//text.setFont(g_font);
		//char buf[100];
		//sprintf_s(buf, "(%d, %d)", avatar.m_x, avatar.m_y);
		//text.setPosition(50.0f, 50.0f);	// 시작좌표 0, 0 좌상단
		//text.setString(buf);
		//g_window->draw(text);
	}
};

OBJECT avatar;
//OBJECT players[MAX_USER];
//OBJECT npcs[NUM_NPC];
unordered_map <int, OBJECT> npcs;

OBJECT white_tile;
//OBJECT black_tile;
OBJECT grass_tile;

sf::Texture *board;
sf::Texture *pieces;
sf::Texture *grass;

void client_initialize()
{
	board = new sf::Texture;
	pieces = new sf::Texture;
	grass = new sf::Texture;
	if (false == g_font.loadFromFile("cour.ttf")) {
		cout << "Font Loading Error!\n";
		while (true);
	}
	board->loadFromFile("chessmap.bmp");
	pieces->loadFromFile("chess2.png");
	grass->loadFromFile("grass1.png");
	white_tile = OBJECT{ *board, 69, 5, TILE_WIDTH, TILE_WIDTH, 0, 0, 0, 0, 0 };
	//black_tile = OBJECT{ *board, 69, 5, TILE_WIDTH, TILE_WIDTH, 0, 0, 0, 0, 0 };
	grass_tile = OBJECT{ *grass, 0, 0, TILE_WIDTH, TILE_WIDTH, 0, 0, 0, 0, 0 };
	avatar = OBJECT{ *pieces, 79, 0, 39, 39, 0, 0, 0, 0, 0 };
	//avatar.move(4, 4); 
	//for (auto &pl : players) {
	//	pl = OBJECT{ *pieces, 64, 0, 64, 64 };
	//}
}

void client_finish()
{
	delete board;
	delete pieces;
}

void ProcessPacket(char *ptr)
{
	static bool first_time = true;
	switch (ptr[1])
	{
	case SC_LOGIN_OK:
	{
		sc_packet_login_ok *packet = reinterpret_cast<sc_packet_login_ok *>(ptr);
		g_myid = packet->id;
		avatar.m_level = packet->level;
		avatar.c_exp = packet->exp;
		avatar.c_hp = packet->hp;
		avatar.m_x = packet->x;
		avatar.m_y = packet->y;
		avatar.is_alive = true;
		avatar.m_exp = 100 + (100 * (avatar.m_level - 1));
		avatar.m_hp = 100 + (20 * (avatar.m_level - 1));
		g_login = true;
		break;
	}
	case SC_LOGIN_FAIL:
	{
		g_login = false;
		break;
	}	
	case SC_PUT_OBJECT:
	{
		sc_packet_put_object *my_packet = reinterpret_cast<sc_packet_put_object *>(ptr);
		int id = my_packet->id;

		if (id == g_myid) {
			avatar.move(my_packet->x, my_packet->y);
			g_left_x = my_packet->x - 9;
			g_top_y = my_packet->y - 9;
			avatar.show();
		}
		else {
			if (id < NPC_ID_START)
				npcs[id] = OBJECT{ *pieces, 119, 0, 39, 39, 0, 0, 0, 0, 0 };
			else {
				switch (npcs[id].m_level) {
				case LEVEL1:
					npcs[id] = OBJECT{ *pieces, 0, 0, 40, 40, 0, 0, LEVEL1, 0, 0 };
					break;
				case LEVEL2: 
					npcs[id] = OBJECT{ *pieces, 39, 0, 40, 40, 0, 0, LEVEL2, 0, 0 };
					break;
				case LEVEL3:
					npcs[id] = OBJECT{ *pieces, 159, 0, 40, 40, 0, 0, LEVEL3, 0, 0 };
					break;
				case LEVEL4:
					npcs[id] = OBJECT{ *pieces, 199, 0, 40, 40, 0, 0, LEVEL4, 0, 0 };
					break;
				}
				
			}
			npcs[id].move(my_packet->x, my_packet->y);
			npcs[id].show();
			npcs[id].is_alive = true;
		}
		break;
	}
	case SC_POS:
	{
		sc_packet_pos *my_packet = reinterpret_cast<sc_packet_pos *>(ptr);
		int other_id = my_packet->id;
		if (other_id == g_myid) {
			avatar.move(my_packet->x, my_packet->y);
			g_left_x = my_packet->x - 9;
			g_top_y = my_packet->y - 9;
		}
		else {
			if (0 != npcs.count(other_id))
				npcs[other_id].move(my_packet->x, my_packet->y);
		}
		break;
	}

	case SC_REMOVE_OBJECT:
	{
		sc_packet_remove_object *my_packet = reinterpret_cast<sc_packet_remove_object *>(ptr);
		int other_id = my_packet->id;
		npcs[other_id].is_alive = false;
		if (other_id == g_myid) {
			avatar.hide();
		}
		else {
			if (0 != npcs.count(other_id))
				npcs[other_id].hide();
		}
		break;
	}
	case SC_CHAT:
	{
		sc_packet_chat *my_packet = reinterpret_cast<sc_packet_chat *>(ptr);
		int other_id = my_packet->id;
		if (other_id == g_myid) {
			avatar.add_chat(my_packet->chat);
		}
		else {
			if (0 != npcs.count(other_id))
				npcs[other_id].add_chat(my_packet->chat);
		}
		break;
	}
	case SC_STAT_CHANGE:
	{
		sc_packet_stat_change *my_packet = reinterpret_cast<sc_packet_stat_change*>(ptr);
		if (my_packet->id == g_myid) {
			avatar.c_exp = my_packet->c_exp;
			avatar.m_exp = my_packet->m_exp;
			avatar.c_hp = my_packet->c_hp;
			avatar.m_hp = my_packet->m_hp;
			avatar.m_level = my_packet->level;
		}
		else {
			int id = my_packet->id;
			npcs[id].c_exp = my_packet->c_exp;
			npcs[id].m_exp = my_packet->m_exp;
			npcs[id].c_hp = my_packet->c_hp;
			npcs[id].m_hp = my_packet->m_hp;
			npcs[id].m_level = my_packet->level;
		}
		break;
	}
	case SC_MONSTER_STAT:
	{
		sc_packet_monster_stat* my_packet = reinterpret_cast<sc_packet_monster_stat *>(ptr);
		int npc_id = my_packet->id;
		npcs[npc_id].display_hp(my_packet->c_hp);
		break;
	}
	//case SC_STAT_CHANGE_MONSTER: 
	//{	
	//	sc_packet_monster_stat_change *my_packet = reinterpret_cast<sc_packet_monster_stat_change*>(ptr);

	//	int npc_id = my_packet->id;
	//	cout << my_packet->c_hp << endl;
	//	npcs[npc_id].c_hp = my_packet->c_hp;

	//	if (npcs[npc_id].c_hp <= 0)
	//		npcs[npc_id].is_alive = false;
	//	break;
	//}
	default:
		printf("Unknown PACKET type [%d]\n", ptr[1]);
	}
}

void process_data(char *net_buf, size_t io_byte)
{
	char *ptr = net_buf;
	static size_t in_packet_size = 0;
	static size_t saved_packet_size = 0;
	static char packet_buffer[BUF_SIZE];

	while (0 != io_byte) {
		if (0 == in_packet_size) in_packet_size = ptr[0];
		if (io_byte + saved_packet_size >= in_packet_size) {
			memcpy(packet_buffer + saved_packet_size, ptr, in_packet_size - saved_packet_size);
			ProcessPacket(packet_buffer);
			ptr += in_packet_size - saved_packet_size;
			io_byte -= in_packet_size - saved_packet_size;
			in_packet_size = 0;
			saved_packet_size = 0;
		} 
		else {
			memcpy(packet_buffer + saved_packet_size, ptr, io_byte);
			saved_packet_size += io_byte;
			io_byte = 0;
		}
	}
}

void client_main()
{
	char net_buf[BUF_SIZE];
	size_t	received;

	auto recv_result = socket.receive(net_buf, BUF_SIZE, received);
	if (recv_result == sf::Socket::Error)
	{
		wcout << L"Recv 에러!";
		while (true);
	}
	if (recv_result != sf::Socket::NotReady)
		if (received > 0) process_data(net_buf, received);

	for (int i = 0; i < SCREEN_WIDTH; ++i)
		for (int j = 0; j < SCREEN_HEIGHT; ++j)	
		{
			int tile_x = i + g_left_x;
			int tile_y = j + g_top_y;
			if ((tile_x < 0) || (tile_y < 0)) continue;

			if (g_map[tile_x][tile_y] == '0'){
				grass_tile.a_move(TILE_WIDTH * i + 7, TILE_WIDTH * j + 7);
				grass_tile.a_draw();
			}
			else if (g_map[tile_x][tile_y] == '1') {
				white_tile.a_move(TILE_WIDTH * i + 7, TILE_WIDTH * j + 7);
				white_tile.a_draw();
			}

			/*if (((tile_x / 3 + tile_y / 3) % 2) == 0 ) {
				white_tile.a_move(TILE_WIDTH * i + 7, TILE_WIDTH * j + 7);
				white_tile.a_draw();
			}
			else
			{
				black_tile.a_move(TILE_WIDTH * i + 7, TILE_WIDTH * j + 7);
				black_tile.a_draw();
			}*/
		}
	avatar.draw();
//	for (auto &pl : players) pl.draw();
	for (auto &npc : npcs) {
		if (false == npc.second.is_alive) continue;
		npc.second.draw();
	}
	sf::Text text;
	text.setFont(g_font);
	char buf[100];
	sprintf_s(buf, "(%d, %d)", avatar.m_x, avatar.m_y);
	text.setPosition(50.0f, 750.0f);	// 시작좌표 0, 0 좌상단
	text.setString(buf);
	g_window->draw(text);

	sf::Text text_avatar_inform;
	text_avatar_inform.setFont(g_font);
	char buf1[100];
	sprintf_s(buf1, "Level: %d\nEXP:%d / %d\nHP: %d / %d\n", avatar.m_level, avatar.c_exp, avatar.m_exp, avatar.c_hp, avatar.m_hp);
	text_avatar_inform.setPosition(570.f, 0.f);
	text_avatar_inform.setString(buf1);
	g_window->draw(text_avatar_inform);
}

void send_packet_move(int p_type, int p_direction)
{
	cs_packet_move packet;
	packet.size = sizeof(packet);
	packet.type = p_type;
	packet.direction = p_direction;
	size_t sent = 0;
	socket.send(&packet, sizeof(packet), sent);
}

void send_packet_attack(int p_type)
{
	cs_packet_attack packet;
	packet.size = sizeof(packet);
	packet.type = p_type;
	size_t sent = 0;
	socket.send(&packet, sizeof(packet), sent);
}

void send_packet_chat(int p_type)
{
	cs_packet_chat packet;
	packet.size = sizeof(packet);
	packet.type = p_type;
	packet.id = g_myid;
	strcpy_s(packet.chat_str, g_chatBuff);
	size_t sent = 0;
	socket.send(&packet, sizeof(packet), sent);
}

void Create_map() 
{
	cout << "Create Map" << endl;

	int xIndex = 0;
	int yIndex = 0;
	char data = 0;
	while (!in.eof()) {
		in >> data;
		g_map[xIndex][yIndex] = data;

		yIndex++;
		if (yIndex == 800) {
			xIndex++;
			yIndex = 0;
		}
	}
	cout << "Finish" << endl;
}

void send_login_pakcet(int id)
{
	cs_packet_login packet;
	packet.size = sizeof(packet);
	packet.type = CS_LOGIN;
	packet.id = id;
	size_t sent = 0;

	socket.send(&packet, sizeof(packet), sent);
}

void recv_login()
{
	char net_buf[BUF_SIZE];
	size_t	received;

	auto recv_result = socket.receive(net_buf, BUF_SIZE, received);
	if (recv_result == sf::Socket::Error)
	{
		wcout << L"Recv 에러!";
		while (true);
	}
	if (recv_result != sf::Socket::NotReady)
		if (received > 0) process_data(net_buf, received);
}

void send_packet_teleport(int p_type, int p_site)
{
	cs_packet_teleport packet;
	packet.size = sizeof(packet);
	packet.type = p_type;
	packet.site = p_site;
	size_t sent = 0;
	socket.send(&packet, sizeof(packet), sent);
}

void send_packet_logout(int p_type)
{
	cs_packet_logout packet;
	packet.size = sizeof(packet);
	packet.type = p_type;
	size_t sent = 0;
	socket.send(&packet, sizeof(packet), sent);
}

int main()
{
	wcout.imbue(locale("korean"));
	sf::Socket::Status status = socket.connect("127.0.0.1", SERVER_PORT);

	if (status != sf::Socket::Done) {
		wcout << L"서버와 연결할 수 없습니다.\n";
		while (true);
	}

	Create_map();
	client_initialize();

	printf("ID 입력 : ");
	scanf_s("%d", &g_tmpid);
	send_login_pakcet(g_tmpid);
	sc_packet_login_check packet;
	size_t received;
	socket.receive(&packet, sizeof(packet), received);
	switch (packet.type) {
	case SC_LOGIN_FAIL:
		return 0;
	case SC_LOGIN_OK:
		sc_packet_login_ok packet_ok;
		size_t received;
		socket.receive(&packet_ok, sizeof(packet_ok), received);
		g_myid = packet_ok.id;
		avatar.m_level = packet_ok.level;
		avatar.c_exp = packet_ok.exp;
		avatar.c_hp = packet_ok.hp;
		avatar.m_x = packet_ok.x;
		avatar.m_y = packet_ok.y;
		avatar.is_alive = true;
		avatar.m_exp = 100 + (100 * (avatar.m_level - 1));
		avatar.m_hp = 100 + (20 * (avatar.m_level - 1));

		cout << avatar.m_x << ", " << avatar.m_y << endl;
		break;
	}

	socket.setBlocking(false);

	sf::RenderWindow window(sf::VideoMode(WINDOW_WIDTH, WINDOW_HEIGHT), "2D CLIENT");
	g_window = &window;

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
			if (event.type == sf::Event::KeyPressed) {
				int p_type = -1;
				int p_direction = -1;
				switch (event.key.code) {
				case sf::Keyboard::Left:
					p_type = CS_MOVE;
					p_direction = LEFT;
					send_packet_move(p_type, p_direction);
					break;
				case sf::Keyboard::Right:
					p_type = CS_MOVE;
					p_direction = RIGHT;
					send_packet_move(p_type, p_direction);
					break;
				case sf::Keyboard::Up:
					p_type = CS_MOVE;
					p_direction = UP;
					send_packet_move(p_type, p_direction);
					break;
				case sf::Keyboard::Down:
					p_type = CS_MOVE;
					p_direction = DOWN;
					send_packet_move(p_type, p_direction);
					break;
				case sf::Keyboard::A:
					p_type = CS_ATTACK;
					send_packet_attack(p_type);
					break;
				case sf::Keyboard::F1:
					p_type = CS_TELEPORT;
					send_packet_teleport(p_type, SITE::A);
					break;
				case sf::Keyboard::F2:
					p_type = CS_TELEPORT;
					send_packet_teleport(p_type, SITE::B);
					break;
				case sf::Keyboard::F3:
					p_type = CS_TELEPORT;
					send_packet_teleport(p_type, SITE::C);
					break;
				case sf::Keyboard::F4:
					p_type = CS_TELEPORT;
					send_packet_teleport(p_type, SITE::D);
					break;
				case sf::Keyboard::Escape:
					p_type = CS_LOGOUT;
					send_packet_logout(p_type);
					window.close();
					break;
				case sf::Keyboard::Enter:
					if (NONE == g_isChat) g_isChat = ING;
					else if (ING == g_isChat) g_isChat = SEND;

					if (SEND == g_isChat) {
						g_isChat = NONE;
						p_type = CS_CHAT;
						send_packet_chat(p_type);
						g_chatIndex = 0;
						for (auto& c : g_chatBuff) c = 0;
					}
					break;
				default:
					if (ING == g_isChat) {
						if (50 > g_chatIndex)
							g_chatBuff[g_chatIndex++] = static_cast<char>(event.key.code + 97);
					}
					break;
				}
			}
		}

		window.clear();
		client_main();
		window.display();
	}
	client_finish();

	return 0;
}